# coding:utf-8
# This is a sample Python script.

# Press Shift+F10 to execute it or replace it with your code.
# Press Double Shift to search everywhere for classes, files, tool windows, actions, and settings.
import random
import threading
import time
import re  # 导入包
import imouse_api as iMouse_api

api = iMouse_api.HttpApi("127.0.0.1")


def findimage(deviceID, filename, isClick, offsetX, offsetY, afor=5, confidence=0.8):
    """
    查找指定图片并执行点击操作。

    Args:
        deviceId (str): 设备ID。
        filename (str): 图片文件名。
        isClick (bool): 是否执行点击操作。
        offsetX (int): 偏移量X。
        offsetY (int): 偏移量Y。
        afor (int, optional): 查找次数。默认为5。
        confidence (float, optional): 置信度阈值。默认为0.8。

    Returns:
        bool: 是否找到图片并执行了点击操作。

    """
    file = open(filename, "rb")
    byte = file.read()
    file.close()
    tmp = False
    for i in range(afor):
        ret = api.find_image(deviceID, byte)
        if ret["status"] == 0 and ret["data"]["code"] == 0 and ret["data"]["confidence"] >= confidence:
            if isClick:
                # api.mouse_movie(deviceID, ret["data"]["result"][0] + offsetX, ret["data"]["result"][1] + offsetY)
                api.click(deviceID, ret["data"]["result"][0] + offsetX, ret["data"]["result"][1] + offsetY, "left")
                time.sleep(0.4)
                api.mouse_movie(deviceID, random.randint(50, 300), random.randint(100, 600))
            else:
                api.mouse_movie(deviceID, ret["data"]["result"][0] + offsetX, ret["data"]["result"][1] + offsetY)
            tmp = True
            break
        else:
            print("find_image", i, filename, ret)
            time.sleep(0.4)
    return tmp


def findocr(deviceID, ocr, txtLst, isClick):
    for item in ocr["data"]["list"]:
        for txtItem in txtLst:
            if txtItem == item["txt"]:
                if isClick:
                    api.click(deviceID, item["result"][0], item["result"][1], "left")
                return True
    return False


def findocr1(deviceID, ocr, txt, isClick):
    ok = False
    for item in ocr["data"]["list"]:
        if item["txt"].find("反馈"):
            ok = True
        if item["txt"].find("跳过") == 0 and (item["txt"].find("s") >= 0 or ok):
            if isClick:
                api.click(deviceID, item["result"][0], item["result"][1], "left")
            return True
    return False


def run(deviceID):
    print(deviceID)
    # api.mouse_movie(deviceID, random.randint(50, 300), random.randint(100, 600))
    # time.sleep(1.5)
    #
    # tmp = findimage(deviceID, "D:/ScreenSrv/ScreenControl/Screenshot/主图标.bmp", True, 0, 0)
    # if not tmp:
    #     return
    # findimage(deviceID, "D:/ScreenSrv/ScreenControl/Screenshot/继续闯关.bmp", True, 0, 0)
    # 恭喜获得-确认领取

    while True:
        api.mouse_movie(deviceID, random.randint(50, 300), random.randint(100, 600))
        time.sleep(0.4)

        ocr = api.ocr(deviceID, [[6, 7], [6, 885], [412, 7], [412, 885]])
        if ocr["status"] == 0 and ocr["data"]["code"] == 0 and len(ocr["data"]["list"]) > 0:
            try:
                print("恭喜获得-确认领取", ocr["data"]["list"][0]["txt"])
                sStr = str(ocr)
                if sStr.find("恭喜获得") >= 0 and sStr.find("恭喜获得") >= 0:
                    findimage(deviceID, "D:/ScreenSrv/ScreenControl/Screenshot/恭喜获得-确认领取.bmp", False, 0, 0, 1)
                elif sStr.find("气泡红包") >= 0 and sStr.find("可提现至微信") >= 0:
                    # findimage(deviceID, "D:/ScreenSrv/ScreenControl/Screenshot/气泡红包-领取.bmp", True, 0, 0, 1)
                    findocr(deviceID, ocr, ["领取"], True)
                elif sStr.find("当前分数") >= 0 and sStr.find("继续闯关") >= 0:
                    findimage(deviceID, "D:/ScreenSrv/ScreenControl/Screenshot/主界面-红包手.bmp", True, 0, 0, 1)
                elif sStr.find("通关奖励") >= 0 and sStr.find("多倍领取") >= 0:
                    findocr(deviceID, ocr, ["多倍领取"], True)

                findocr(deviceID, ocr, ["残忍离开"], True)
                findocr1(deviceID, ocr, "", True)
            except:
                print('OCR异常')

        tmp = findimage(deviceID, "D:/ScreenSrv/ScreenControl/Screenshot/通关-多倍领取.bmp", True, 0, 0, 1)
        if tmp:
            time.sleep(random.randint(1, 3))
        # print("swipe", i)

        findimage(deviceID, "D:/ScreenSrv/ScreenControl/Screenshot/通用-关闭.bmp", True, 0, 0, 1)
        findimage(deviceID, "D:/ScreenSrv/ScreenControl/Screenshot/通用-关闭1.bmp", True, 0, 0, 1)
        findimage(deviceID, "D:/ScreenSrv/ScreenControl/Screenshot/通用-关闭3.bmp", True, 0, 0, 1)

        tmp = findimage(deviceID, "D:/ScreenSrv/ScreenControl/Screenshot/恭喜获得-确认领取.bmp", False, 0, 0, 1)
        if tmp:
            ocr = api.ocr(deviceID, [[126, 299], [126, 348], [308, 299], [308, 348]])
            if ocr["status"] == 0 and ocr["data"]["code"] == 0 and len(ocr["data"]["list"]) > 0:
                print("恭喜获得-确认领取", ocr["data"]["list"][0]["txt"])
            findimage(deviceID, "D:/ScreenSrv/ScreenControl/Screenshot/恭喜获得-确认领取.bmp", True, 0, 0, 1)
        # time.sleep(random.randint(1, 3))
        findimage(deviceID, "D:/ScreenSrv/ScreenControl/Screenshot/恭喜过关-继续闯关.bmp", True, 0, 0, 1)

        findimage(deviceID, "D:/ScreenSrv/ScreenControl/Screenshot/气泡红包-领取.bmp", True, 0, 0, 1)
        findimage(deviceID, "D:/ScreenSrv/ScreenControl/Screenshot/通用-关闭2.bmp", True, 0, 0, 1)

        findimage(deviceID, "D:/ScreenSrv/ScreenControl/Screenshot/可提现-立即拆开.bmp", True, 0, 0, 1)

        findimage(deviceID, "D:/ScreenSrv/ScreenControl/Screenshot/通用-关闭4.bmp", True, 0, 0, 1)
        findimage(deviceID, "D:/ScreenSrv/ScreenControl/Screenshot/通用-关闭5.bmp", True, 0, 0, 1)
        findimage(deviceID, "D:/ScreenSrv/ScreenControl/Screenshot/通用-关闭6.bmp", True, 0, 0, 1)
        findimage(deviceID, "D:/ScreenSrv/ScreenControl/Screenshot/通用-关闭7.bmp", True, 0, 0, 1)
        findimage(deviceID, "D:/ScreenSrv/ScreenControl/Screenshot/弹出-完成.bmp", True, 0, 0, 1)
        # findimage(deviceID, "D:/ScreenSrv/ScreenControl/Screenshot/通用-关闭8.bmp", True, 0, 0, 1)
        findimage(deviceID, "D:/ScreenSrv/ScreenControl/Screenshot/通用-关闭9.bmp", True, 0, 0, 1)
        findimage(deviceID, "D:/ScreenSrv/ScreenControl/Screenshot/反馈-关闭.bmp", True, 50, -3, 1)
        findimage(deviceID, "D:/ScreenSrv/ScreenControl/Screenshot/广告-放弃福利.bmp", True, 0, 0, 1)
        findimage(deviceID, "D:/ScreenSrv/ScreenControl/Screenshot/跳过.bmp", True, 0, 0, 1)
        findimage(deviceID, "D:/ScreenSrv/ScreenControl/Screenshot/跳过1.bmp", True, 0, 0, 1)
        findimage(deviceID, "D:/ScreenSrv/ScreenControl/Screenshot/广告-不感兴趣.bmp", True, 0, 0, 1)
        findimage(deviceID, "D:/ScreenSrv/ScreenControl/Screenshot/通用-关闭A.bmp", True, 0, 0, 1)
        findimage(deviceID, "D:/ScreenSrv/ScreenControl/Screenshot/通用-关闭B.bmp", True, 0, 0, 1)
        findimage(deviceID, "D:/ScreenSrv/ScreenControl/Screenshot/通用-关闭c.bmp", True, 0, 0, 1)
        findimage(deviceID, "D:/ScreenSrv/ScreenControl/Screenshot/通用-关闭d.bmp", True, 0, 0, 1)
        findimage(deviceID, "D:/ScreenSrv/ScreenControl/Screenshot/通用-关闭e.bmp", True, 0, 0, 1)
        findimage(deviceID, "D:/ScreenSrv/ScreenControl/Screenshot/取消.bmp", True, 0, 0, 1)

        findimage(deviceID, "D:/ScreenSrv/ScreenControl/Screenshot/主界面-红包手.bmp", True, 0, 0, 1)
    # 可提现-立即拆开  主界面-红包手

    # 用当前分数来定位，好找下面的广告来点击
    # tmp = findimage(deviceID, "D:/ScreenSrv/ScreenControl/Screenshot/主页-当前分数.bmp", True, -16, 68, 1, 0.9)
    # if tmp:
    #     time.sleep(random.randint(1, 3))
    print("Exit Thread.")


if __name__ == '__main__':

    deviceidLst = []
    deviceidLst.append('88:64:40:06:C6:53')  # 群控多台手机1
    # deviceidLst.append('DC:2B:2A:14:2A:F6')   #群控多台手机2
    # deviceidLst.append('DC:2B:2A:14:2A:F2')   #群控多台手机3
    # deviceidLst.append('DC:2B:2A:14:2A:F3')   #群控多台手机4
    for deviceId in deviceidLst:
        thread1 = threading.Thread(target=run, args=(deviceId,))
        thread1.start()
        time.sleep(1)

    while True:
        cmd = input("请输入:")
        if cmd == "q":
            break
